package com.relese.aashditmt.movie_list;

import static com.relese.aashditmt.api_controller.ApiMethodClasses.API_FAILED_ERROR_STATUS_CODE;
import static com.relese.aashditmt.utils.Constants.MOVIE_VIEW_TYPE;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.relese.aashditmt.R;
import com.relese.aashditmt.adapters.MovieListAdapter;
import com.relese.aashditmt.api_controller.Controller;
import com.relese.aashditmt.custom_helper_classes.MovieListBaseActivity;
import com.relese.aashditmt.outputmodel.APIResultModel;
import com.relese.aashditmt.outputmodel.MovieListModel;
import com.relese.aashditmt.outputmodel.MovieListOutputModel;

import java.util.ArrayList;

public class MovieListActivity extends MovieListBaseActivity {
    private RecyclerView movieListRecyclerView;
    private ArrayList<Object> movieList;
    private MovieListAdapter movieListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);

        movieListRecyclerView = findViewById(R.id.movieListRecyclerView);

        showProgress();
        Controller.getMovieList(this,new Controller.ApiControllerCallback() {
            @Override
            public void getDataFromApi(APIResultModel apiResultModel) {
                // response
                dismissProgress();
                if (apiResultModel.getStatusCode() == API_FAILED_ERROR_STATUS_CODE){
                    Toast.makeText(MovieListActivity.this, apiResultModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else{
                    if (apiResultModel.getAPIOutputModel() != null){
                        ArrayList<MovieListModel> movieListOutputModel = (ArrayList<MovieListModel>) apiResultModel.getAPIOutputModel();
                        movieList = new ArrayList<Object>(movieListOutputModel);
                        movieListAdapter = new MovieListAdapter(MovieListActivity.this, movieList, R.layout.movie_list_row, MOVIE_VIEW_TYPE, new MovieListAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(Object item) {

                            }
                        });
                        movieListRecyclerView.setLayoutManager(new LinearLayoutManager(MovieListActivity.this,LinearLayoutManager.VERTICAL,false));
                        movieListRecyclerView.setAdapter(movieListAdapter);
                    }
                }
            }
        });
    }
}