package com.relese.aashditmt.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;
import com.relese.aashditmt.R;
import com.relese.aashditmt.movie_list.MovieListActivity;

public class SplashActivity extends AppCompatActivity {
    private TextView versionNameTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        versionNameTextView = findViewById(R.id.versionNameTextView);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            versionNameTextView.setText("v " +version);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Thread timer= new Thread() {
            public void run() {
                try {
                    //Display for 3 seconds
                    sleep(3000);
                } catch (InterruptedException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                } finally {
                    startActivity(new Intent(SplashActivity.this, MovieListActivity.class));
                    finish();
                }
            }
        };
        timer.start();
    }
}