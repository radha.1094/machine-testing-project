package com.relese.aashditmt.adapters;

import static com.relese.aashditmt.utils.Constants.MOVIE_VIEW_TYPE;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.relese.aashditmt.R;
import com.relese.aashditmt.outputmodel.MovieListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MovieListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    ArrayList<Object> movieListModelArrayList;
    public interface OnItemClickListener {
        void onItemClick(Object item);
    }
    private final OnItemClickListener listener;
    private Context context;
    int layoutId;
    int viewType = MOVIE_VIEW_TYPE;


    public MovieListAdapter(Context context, ArrayList<Object> movieListModelArrayList, int layoutId, int viewType, OnItemClickListener listener) {
        this.context = context;
        this.movieListModelArrayList = movieListModelArrayList;
        this.layoutId = layoutId;
        this.viewType = viewType;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        if (i == MOVIE_VIEW_TYPE) {
            return new MovieListViewHolder(itemView);
        }
        return null;
    }
    @Override
    public int getItemViewType(int position) {
        return viewType;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        viewHolder.setIsRecyclable(false);
        if (viewHolder.getItemViewType() == MOVIE_VIEW_TYPE) {
            MovieListViewHolder movieListViewHolder = (MovieListViewHolder) viewHolder;
            MovieListModel movieListModel = (MovieListModel) movieListModelArrayList.get(position);

            movieListViewHolder.posterDuration.setText(movieListModel.getRuntime());
            movieListViewHolder.posterYear.setText(movieListModel.getYear());
            movieListViewHolder.posterName.setText(movieListModel.getTitle());

            if(movieListModel.getPoster() != null && !movieListModel.getPoster().matches("")){
                Picasso.get()
                        .load(movieListModel.getPoster())
                        .placeholder(R.drawable.baseline_image_not_supported_24)
                        .error(R.drawable.baseline_image_not_supported_24)
                        .into(movieListViewHolder.posterImageView);
            }else{
                Picasso.get()
                        .load(movieListModel.getPoster())
                        .placeholder(R.drawable.baseline_image_not_supported_24)
                        .error(R.drawable.baseline_image_not_supported_24)
                        .into(movieListViewHolder.posterImageView);
                movieListViewHolder.posterImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
        }
    }

    @Override
    public int getItemCount() {
        return movieListModelArrayList.size();
    }

    public class MovieListViewHolder extends RecyclerView.ViewHolder {
        private TextView posterDuration,posterYear,posterName;
        private ImageView posterImageView;
        public MovieListViewHolder(@NonNull View itemView) {
            super(itemView);

            posterImageView = itemView.findViewById(R.id.posterImageView);
            posterDuration = itemView.findViewById(R.id.posterDuration);
            posterYear = itemView.findViewById(R.id.posterYear);
            posterName = itemView.findViewById(R.id.posterName);
        }
    }
}