package com.relese.aashditmt.api_controller;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkHttpHelper {

    public static final String CONTENT_TYPE_ENCODED = "application/x-www-form-urlencoded; charset=UTF-8";
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final MediaType MEDIA_URL_ENCODED = MediaType.parse(CONTENT_TYPE_ENCODED);
    public static final MediaType MEDIA_JSON = MediaType.parse(CONTENT_TYPE_JSON);
    private static OkHttpClient okHttpClient;

    private static void initOkHttp() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        try {
            long timeout = 17 * 1000L;
            builder.connectTimeout(timeout, TimeUnit.MILLISECONDS)
                    .readTimeout(timeout, TimeUnit.MILLISECONDS)
                    .writeTimeout(timeout, TimeUnit.MILLISECONDS);
        } catch (Exception e) {}
        okHttpClient = builder.build();
    }


    /**
     * This method returns a singleton instance of OkHttpClient object.
     * @return
     */
    public static OkHttpClient getClientInstance() {
        if (okHttpClient==null) initOkHttp();
        return okHttpClient;
    }


    public static void enqueueGET(LinkedHashMap<String, String> parameters, String url, EnqueueCallback callback) {
        enqueueGET(null, parameters, url, callback);
    }

    public static void enqueueGET(String url, EnqueueCallback callback) {
        enqueueGET(null, null, url, callback);
    }

    /**
     * Simple GET request
     * @param parameters
     * @param headers
     * @param url
     * @param callback
     */

    public static void enqueueGET(LinkedHashMap<String, String> parameters, LinkedHashMap<String, String> headers, String url, final EnqueueCallback callback) {
        final Handler handler = new Handler(Looper.getMainLooper());
        try {
            HttpUrl.Builder httpUrlBuilder = HttpUrl.parse(url).newBuilder();
            if (parameters!=null) {
                Set<Map.Entry<String, String>> entrySet = parameters.entrySet();
                for (Map.Entry<String, String> entry : entrySet) {
                    if (!TextUtils.isEmpty(entry.getKey()))
                        httpUrlBuilder
                                .addQueryParameter(entry.getKey(), (entry.getValue()!=null)? entry.getValue(): "");
                }
            }

            Request.Builder requestBuilder = new Request.Builder();

            if (headers!=null) {
                Set<Map.Entry<String, String>> entrySet = headers.entrySet();
                for (Map.Entry<String, String> entry : entrySet) {
                    if (!TextUtils.isEmpty(entry.getKey()))
                        requestBuilder.addHeader(entry.getKey(), (entry.getValue()!=null)? entry.getValue(): "");
                }
            }

            Request request = requestBuilder
                    .url(httpUrlBuilder.build())
                    .build();

            getClientInstance().newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onComplete(null, 0);
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        final String value = response.body().string();
                        final int code = response.code();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onComplete(value, code);
                            }
                        });
                    } catch (Exception e) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onComplete(null, 0);
                            }
                        });
                    }
                }
            });
        } catch (Exception e) {
            if (callback!=null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onComplete(null, 0);
                    }
                });
            }
        }


    }

    public static void enqueuePOST(JSONObject postBody, LinkedHashMap<String, String> headers, String url, EnqueueCallback callback) {
        if (postBody!=null) enqueuePOST(postBody.toString(), headers, url, callback);
        else enqueuePOST((String) null, headers, url, callback);
    }
    public static void  enqueuePOST(String postBody, LinkedHashMap<String, String> headers, String url, final EnqueueCallback callback) {
        if (callback==null) return;
        final Handler handler = new Handler(Looper.getMainLooper());
        try {
            HttpUrl.Builder httpUrlBuilder = HttpUrl.parse(url).newBuilder();

            Request.Builder requestBuilder = new Request.Builder();

            if (headers!=null) {
                Set<Map.Entry<String, String>> entrySet = headers.entrySet();
                for (Map.Entry<String, String> entry : entrySet) {
                    if (!TextUtils.isEmpty(entry.getKey()))
                        requestBuilder.addHeader(entry.getKey(), (entry.getValue()!=null)? entry.getValue(): "");
                }
            }


            if (postBody==null) {
                postBody = "";
            }

            RequestBody body = RequestBody.create(MEDIA_JSON, postBody);

            Request request = requestBuilder
                    .url(httpUrlBuilder.build())
                    .post(body)
                    .build();

            getClientInstance().newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onComplete(null, 0);
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        final String value = response.body().string();
                        final int code = response.code();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onComplete(value, code);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onComplete(null, 0);
                            }
                        });
                    }
                }
            });
        }catch (Exception e ){
            e.printStackTrace();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    callback.onComplete(null, 0);
                }
            });
        }
    }

    public static void enqueueDELETE(JSONObject deleteBody, LinkedHashMap<String, String> headers, String url, EnqueueCallback callback) {
        if (deleteBody!=null) enqueueDELETE(deleteBody.toString(), headers, url, callback);
        else enqueueDELETE((String) null, headers, url, callback);
    }
    public static void enqueueDELETE(String deleteBody, LinkedHashMap<String, String> headers, String url, final EnqueueCallback callback) {
        if (callback==null) return;
        final Handler handler = new Handler(Looper.getMainLooper());
        try {
            HttpUrl.Builder httpUrlBuilder = HttpUrl.parse(url).newBuilder();

            Request.Builder requestBuilder = new Request.Builder();

            if (headers!=null) {
                Set<Map.Entry<String, String>> entrySet = headers.entrySet();
                for (Map.Entry<String, String> entry : entrySet) {
                    if (!TextUtils.isEmpty(entry.getKey()))
                        requestBuilder.addHeader(entry.getKey(), (entry.getValue()!=null)? entry.getValue(): "");
                }
            }

            if (deleteBody==null) {
                deleteBody = "";
            }

            RequestBody body = RequestBody.create(MEDIA_JSON, deleteBody);

            Request request = requestBuilder
                    .url(httpUrlBuilder.build())
                    .delete(body)
                    .build();

            getClientInstance().newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onComplete(null, 0);
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        final String value = response.body().string();
                        final int code = response.code();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onComplete(value, code);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onComplete(null, 0);
                            }
                        });
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    callback.onComplete(null, 0);
                }
            });
        }
    }

    public interface EnqueueCallback {
        public void onComplete(String response, int statusCode);
    }
}
