package com.relese.aashditmt.api_controller;

public class ApiMethodClasses {
    public static final String BASE_URL = "https://my-json-server.typicode.com/horizon-code-academy/fake-movies-api/movies";
    public static final int API_FAILED_ERROR_STATUS_CODE = 0;
    public static final int API_SUCCESS_STATUS_CODE = 1;
}