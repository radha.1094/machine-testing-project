package com.relese.aashditmt.api_controller;

import static com.relese.aashditmt.api_controller.ApiMethodClasses.API_FAILED_ERROR_STATUS_CODE;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.relese.aashditmt.R;
import com.relese.aashditmt.network.ConnectivityAndInternetAccessCheck;
import com.relese.aashditmt.outputmodel.APIResultModel;
import com.relese.aashditmt.outputmodel.MovieListModel;
import com.relese.aashditmt.outputmodel.MovieListOutputModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public  class Controller {
    public interface ApiControllerCallback {
        void getDataFromApi(APIResultModel gstResultModel);
    }
    public static void getMovieList(final Context context, final ApiControllerCallback callback) {
        final APIResultModel apiResultModel = new APIResultModel();
        if (ConnectivityAndInternetAccessCheck.isConnected(context)){
            OkHttpHelper.enqueueGET(ApiMethodClasses.BASE_URL, new OkHttpHelper.EnqueueCallback() {
                @Override
                public void onComplete(String response, int statusCode) {
                    apiResultModel.setStatusCode(statusCode);
                    try {
                        JSONArray dataObject =  new JSONArray(response);
                        Gson gson= new Gson();
                        Type collectionType = new TypeToken<ArrayList<MovieListModel>>(){}.getType();
                        ArrayList<MovieListModel> lcs = (ArrayList<MovieListModel>) gson.fromJson(dataObject.toString() , collectionType);
                        apiResultModel.setAPIOutputModel(lcs);
                        callback.getDataFromApi(apiResultModel);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callback.getDataFromApi(new APIResultModel(statusCode,null,context.getResources().getString(R.string.exception_api_message)));
                    }
                }
            });
        }else{
            callback.getDataFromApi(new APIResultModel(API_FAILED_ERROR_STATUS_CODE,null,context.getResources().getString(R.string.check_internet_message)));
        }
    }
}