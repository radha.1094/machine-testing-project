package com.relese.aashditmt.outputmodel;

import java.util.ArrayList;

public class MovieListOutputModel {
    private ArrayList<MovieListModel> movieArrayList;

    public MovieListOutputModel() {
    }

    public ArrayList<MovieListModel> getMovieArrayList() {
        return movieArrayList;
    }

    public void setMovieArrayList(ArrayList<MovieListModel> movieArrayList) {
        this.movieArrayList = movieArrayList;
    }
}
