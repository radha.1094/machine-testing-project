package com.relese.aashditmt.outputmodel;

public class APIResultModel {
    private int statusCode;
    private Object apiOutputModel;
    private String message;

    public APIResultModel() {
    }

    public APIResultModel(int statusCode, Object apiOutputModel,String message) {
        this.statusCode = statusCode;
        this.apiOutputModel = apiOutputModel;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Object getAPIOutputModel() {
        return apiOutputModel;
    }

    public void setAPIOutputModel(Object apiOutputModel) {
        this.apiOutputModel = apiOutputModel;
    }
}
