package com.relese.aashditmt.custom_helper_classes;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.relese.aashditmt.R;

public class MovieListBaseActivity extends AppCompatActivity {
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list_base);
    }

    public Dialog getProgressDialog() {
        if (progressDialog==null) {
            progressDialog = new Dialog(this);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setContentView(R.layout.dialog_loader);
            progressDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

        }
        return progressDialog;
    }
    public void showProgress() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isFinishing() && getProgressDialog() != null && !getProgressDialog().isShowing()) {
                        getProgressDialog().show();
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void dismissProgress() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (! isFinishing() && getProgressDialog().isShowing()) {
                    try {
                        getProgressDialog().dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}